source("functions/step.R")

createPipeline <- function(data, num_of_steps, outer_folds, TRAIN_DATA_PATH,
                           STEP_OUTPUT_PATH, STEP_CLASSES_PATH, pipeline_seed = NULL){
  # Creates a new pipeline
  #
  # Args:
  #   dataset: character. Filename of the dataset. This must be unique.
  #   steps: integer. The number of steps to include in the pipeline.
  #   outer_folds: integer. 2+
  #
  # Returns:
  #   A pipeline object
  
  # choose a seed if none was set explicitly
  if(is.null(pipeline_seed)) {
    pipeline_seed <- floor(runif(1, 1, 1e6))
  }
  set.seed(pipeline_seed)
  
  # add step classes to each step
  step_classes <- sapply(1:num_of_steps, addStepClass, max=num_of_steps)
  
  # select the scripts to use for each step_class
  step_scripts <- sapply(step_classes, selectStepScript, STEP_CLASSES_PATH=STEP_CLASSES_PATH)
  
  # append folds to dataset as part of pipeline creation.
  data$fold <- appendFolds(nrow(data), outer_folds)
  
  # translate step info to single string for the database fields
  TRAIN_DATA_PATH <- paste("'", TRAIN_DATA_PATH, "'", sep="")
  step_classes <- paste("'", paste(step_classes, collapse = "&"), "'", sep="")
  step_scripts <- paste("'", paste(step_scripts, collapse = "&"), "'", sep="")
  
  # write an entry to the pipe table with dataset, num_of_steps, outer_folds, step_classes
  # get pipe_id from database
  pipe_id <- insertPipeline(TRAIN_DATA_PATH, num_of_steps, outer_folds, step_classes, 
                            step_scripts, pipeline_seed)
  print(pipe_id)
  
  # save dataset to be used in first step of the pipeline
  saveData(data, STEP_OUTPUT_PATH, pipe_id, "0")
  
  return(list(as.integer(pipe_id), num_of_steps))
}

addStepClass <- function(step_number, max){
  # data-imputation
  # dim-reduction
  # feature-creation
  # feature-selection
  # fit-predict
  
  if (step_number == max){
    step <- "fit-predict"
  } else if (step_number == 1){
    step <- "data-imputation"
  } else {
    step_chance <- runif(1)
    step <- ifelse(step_chance <= 0.33, "dim-reduction", 
                   ifelse(step_chance > 0.33 & step_chance <= 0.66, "feature-creation",
                          "feature-selection"))
  }
  
  return(step)
}

selectStepScript <- function(x, STEP_CLASSES_PATH){
  step_script <- list.files(paste0(STEP_CLASSES_PATH, x))
  
  return(step_script)
}

insertPipeline <- function(TRAIN_DATA_PATH, num_of_steps, outer_folds, 
                           step_classes, step_scripts, pipeline_seed){
  
  # insert an entry to the pipeline database table
  query <- paste0("
      INSERT INTO pipeline
      (dataset, num_of_steps, outer_folds, step_classes, step_scripts, pipeline_seed, f1)
      VALUES
      (", paste(TRAIN_DATA_PATH, num_of_steps, outer_folds, step_classes, step_scripts, pipeline_seed,
                "NULL)", sep = ","))

  dbExecute(conn, query)
  
  # return the pipe_id
  return(dbGetQuery(conn, "SELECT max(pipeline_id) from pipeline"))
}

runPipeline <- function(pipe_id, num_of_steps, STEP_OUTPUT_PATH){
  
  # check max of steps which have been run for the pipeline (in step output dir)
  # todo: add some error handling in case you run the pipeline steps in a different order
  pattern <- paste0("pipe", pipe_id, "_.*")
  step_files <- list.files(paste0(STEP_OUTPUT_PATH))
  latest_step <- step_files[grep(pattern, step_files)]
  previous_step_id <- strsplit(latest_step, "step")[[1]][2]
  previous_step_id <- strsplit(previous_step_id, "\\.")[[1]][1]
  
  print(paste0("Last step completed was ", previous_step_id))
                           
  # run all steps
  lapply(1:num_of_steps, runStep, pipe_id=pipe_id, previous_step_id=previous_step_id, 
         STEP_OUTPUT_PATH=STEP_OUTPUT_PATH, num_of_steps=num_of_steps)
  
  print("Pipeline complete.")
}

scorePipeline <- function(pipe_id, error_function, STEP_OUTPUT_PATH) {
  
  # get the final step predictions
  steps <- dbGetQuery(conn, paste0("SELECT num_of_steps from pipeline WHERE pipeline_id =", pipe_id))
  data <- loadData(paste0(STEP_OUTPUT_PATH, "pipe", pipe_id, "_step", as.integer(steps), ".feather"))
  names(data)[ncol(data)] <- 'predictions'

  # take the predictions in the final column and apply an error function to it
  
  # categorisation accuracy
  if(error_function == "accuracy"){
    
    # make sure predictions are classes
    predictions <- as.integer(data$predictions)
    
    errors <- ifelse(data[, get(TARGET)] == predictions, 1, 0)
    
    print(paste0("Accuracy: ", 
                 sum(errors[errors == 1])/length(errors)))
  }
  
  #todo: update pipeline DB entry with the score
  
}

loadPipeline <- function(pipe_id){
  # todo. Get an existing pipe by its pipeline_id
}
