library(data.table)
library(feather)

loadData <- function(filename){
  
  # read in data
  data <- data.table(feather::read_feather(filename))
  
  return(data)
}

saveData <- function(data, STEP_OUTPUT_PATH, pipe_ID, step){
  
  # write data
  feather::write_feather(data, paste0(STEP_OUTPUT_PATH, "pipe", pipe_ID, "_step", step, ".feather"))
  print("Dataset saved.")
}

appendFolds <- function(rows, folds){
  
  # assign a fold to each row
  fold_indices <- sample(1:folds, rows, replace = T)
  
  return(fold_indices)
}