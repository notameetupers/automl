library(yaml)
library(DBI)
library(RSQLite)

params <- yaml.load_file("SETTINGS.yaml")
DATABASE_LOCATION <- params$DATABASE_LOCATION

# create step output path directory to ensure pipeline steps can run
dir.create(params$STEP_OUTPUT_PATH, showWarnings = FALSE)

conn <- dbConnect(RSQLite::SQLite(), DATABASE_LOCATION)

# create steps table
dbExecute(conn, "
  CREATE TABLE IF NOT EXISTS steps (
    step_id INTEGER PRIMARY KEY AUTOINCREMENT,
    pipeline_id INTEGER,
    dataset NVARCHAR,
    random_seed INTEGER,
    step_num INTEGER,
    step_name VARCHAR,
    step_options VARCHAR,
    time_taken REAL,
    step_object BLOB,
    -- ...
    f1 REAL
  )
")

# create pipeline table
dbExecute(conn, "
  CREATE TABLE IF NOT EXISTS pipeline (
    pipeline_id INTEGER PRIMARY KEY AUTOINCREMENT,
    dataset NVARCHAR,
    num_of_steps INTEGER,
    outer_folds INTEGER,
    step_classes VARCHAR,
    step_scripts NVARCHAR,
    pipeline_seed INTEGER,
    -- ...
    f1 REAL, 
    accuracy REAL,
    precision REAL, 
    recall REAL
  )
")

# check what's in the database already
dbListTables(conn)

# remove tables
#dbRemoveTable(conn, "pipeline")
#dbRemoveTable(conn, "steps")

dbDisconnect(conn)

