# Step-class: fit-predict
# Step-name: fp_xgboost
# Description: Builds a XGBoost model

# Parameters:



fp_xg_boost <- function(data, random_seed, pipeline_id, STEP_DATA_PATH, step_num, fold = NULL, ...) {
  
  # install.packages('caret')
  # install.packages('randomForest')
  # install.packages('xgboost')
  library('caret')
  library('xgboost')
  library('dplyr')
  
  # Select the datset fold if one is supplied
  if (!is.null(fold)) {
    data <- data[fold_indices == fold]
  }
  
  # TODO: Put these tuning parameters in the YAML file
  xgb_grid <- expand.grid(
    eta = 0.12,
    max_depth = 10,
    nrounds = 400,
    subsample = 1,
    gamma = 0,               #default=0
    colsample_bytree = 1,    #default=1
    min_child_weight = 1     #default=1
  )
  
  
  xgb_trcontrol <- trainControl(
    method="cv",
    number = 5,
    verboseIter = TRUE,
    returnData=FALSE,
    returnResamp = "all",
    allowParallel = TRUE
  )
  
  # TODO
  # Refine this so it's only picking up the correct x_variables
  x_variables <- setdiff(colnames(data), TARGET)
  
  glmnet.formula <- as.formula(paste0(TARGET,' ~ ',paste(x_variables, collapse = ' + ')))
  
  # Start time recording
  start.time <- Sys.time()
  
  xgb_train <- train(form = glmnet.formula,
                     data = data,
                     trControl = xgb_trcontrol,
                     tuneGrid = xgb_grid,
                     method="xgbTree"
  )
  
  #browser()
  
  xgb_pred <- predict(xgb_train, data)
  data.model <- cbind(data, xgb_pred)
  
  # End time recording
  end.time <- Sys.time()
  time_taken <- end.time - start.time
  
  step_options <- list(x_vars = x_variables)
  step_options <- paste("'", paste(names(step_options),step_options, sep = '=', collapse = '&'), "'", sep="")
  
  model_output <- paste0(STEP_OUTPUT_PATH, "pipe", pipeline_id, "_", "step", step_num,"fp_xg_boost", ".RDS")
  saveRDS(object = xgb_train, file = model_output)
  
  
  model_output_sql <- paste0("'",model_output,"'")
  step_id <- insertStep(pipeline_id, STEP_DATA_PATH, random_seed, step_num,
                        step_name = "'fp_xg_boost'", step_options, time_taken, step_object = model_output_sql, f1)
  print(step_id)
  
  return(data.model)
  
}